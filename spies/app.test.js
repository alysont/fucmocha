const expect = require('expect');

const db = {
  saveUser: expect.createSpy(),
};

const app = require('./app')(db);

describe('App', () => {
  it('should call the spy correctly', () => {
    const spy = expect.createSpy();
    spy('Andrew', 25);
    expect(spy).toHaveBeenCalledWith('Andrew', 25);
  });

  it('should call saveUser with user object', () => {
    const email = 'sashimi@example.com';
    const password = '123abc';

    app.handleSignup(email, password);
    expect(db.saveUser).toHaveBeenCalledWith({
      email,
      password,
    });
  });
});
