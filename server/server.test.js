const request = require('supertest');
const expect = require('expect');

const app = require('./server').app;

// server
//  GET /
//    some test
//  GET /users
//    some test

describe('Server', () => {
  describe('GET /', () => {
    it('should return hell awaits response', (done) => {
      request(app)
        .get('/')
        .expect(404)
        .expect((res) => {
          expect(res.body).toInclude({
            error: 'Page Not Found',
          });
        })
        .end(done);
    });
  });

  describe('GET /users', () => {
    it('should return users response', (done) => {
      request(app)
        .get('/users')
        .expect(200)
        .expect((res) => {
          expect(res.body).toInclude({
            name: 'Andrew',
            age: 25,
          });
        })
        .end(done);
    });
  });
});
