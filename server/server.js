
const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.status(404).send({
    error: 'Page Not Found',
    name: 'Todo App v1.0',
  });
});

app.get('/users', (req, res) => {
  res.json([
    {
      name: 'Andrew',
      age: 25,
    },
    {
      name: 'Sashimi',
      age: 33,
    },
  ]);
});

app.listen(3000);

module.exports.app = app;
