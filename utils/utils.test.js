const expect = require('expect');

const utils = require('./utils');

describe('Utils', () => {
  describe('#add', () => {
    it('should add two numbers', () => {
      const res = utils.add(33, 11);
      expect(res).toBe(44).toBeA('number');
    });

    it('should async add two numbers', (done) => {
      utils.asyncAdd(4, 3, (sum) => {
        expect(sum).toBe(7).toBeA('number');
        done();
      });
    });
  });

  it('should square number', () => {
    const res = utils.square(4);
    expect(res).toBe(16).toBeA('number');
  });

  it('should async square number', (done) => {
    utils.asyncSquare(3, (res) => {
      expect(res).toBe(9).toBeA('number');
      done();
    });
  });

  it('should verify first and last names are set', () => {
    const user = { id: 1 };
    const res = utils.setName(user, 'Sashimi Udon');
    expect(res).toInclude({
      firstName: 'Sashimi',
      lastName: 'Udon',
    });
  });

  it('should expect some values', () => {
    // expect(12).toNotBe(11);
    // expect({ name: 'Andrew' }).toEqual({ name: 'Andrew' });
    // expect([2, 3, 4]).toExclude(5);
    expect({
      name: 'Andrew',
      age: 25,
      location: 'Philadelphia',
    }).toExclude({
      age: 24,
    });
  });
});
